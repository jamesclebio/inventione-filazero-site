; (function () {
  'use strict';

  angular
    .module('filazero-site').
    controller('SignupModalController', SignupModalController);

  SignupModalController.$inject = [
    '$state',
    'ngAuthSettings',
    '$modalInstance'
  ];

  function SignupModalController($state, ngAuthSettings, $modalInstance) {
    var vm = this;

    vm.appUrl = ngAuthSettings.urlOrigin;

    vm.goToUserSignup = goToUserSignup;
    vm.goToProviderSignup = goToProviderSignup;

    function goToUserSignup() {
      $state.go('app.signup', {}, { reload: true });
      $modalInstance.close();
    }

    function goToProviderSignup() {
      $modalInstance.close();
    }
  }
})();
