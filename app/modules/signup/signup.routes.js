;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .config(signUpRoutes);

  signUpRoutes.$inject = ['$stateProvider'];

  function signUpRoutes($stateProvider) {
    $stateProvider
      .state('app.signup', {
        url: '/signup?i&e?f',
        templateUrl: 'modules/signup/signup.html',
        controller: 'SignUpController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/signup/signup.controller.js',
                'services/invites.service.js'
              ]);
            });
          }]
        }
      });
  }

}());
