;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .controller('SignUpController', SignUpController);

  SignUpController.$inject = [
    '$scope',
    'AuthService',
    '$stateParams',
    'ngAuthSettings'
  ];

  function SignUpController($scope, AuthService, $stateParams, ngAuthSettings) {

    if($scope.$parent === null){
      $scope.page.setTitle('Cadastre-se');
      $scope.page.setDescription();
    }

    $scope.appUrl = ngAuthSettings.urlOrigin;

    $scope.registration = {
      firstName: "",
      lastName: "",
      email: "",
      checkTerms: false
    };

    if ($stateParams.e) {
      $scope.registration.email = $stateParams.e;
      $scope.hasEmail = true;
    }

    if ($stateParams.f) {
      $scope.registration.firstName = $stateParams.f;
    }

    $scope.checkPassword = function(){
      return $scope.registration.password === $scope.registration.confirmPassword;
    };

    $scope.register = function () {
      $scope.isRunningRegister = true;
      $scope.registration.email = $scope.registration.email.toLowerCase();
      AuthService.saveRegistration($scope.registration).then(
        function (response) {
          if (response.data.messages[0].type !== "SUCCESS") {
            $scope.error = true;
          }
          else {
            if($scope.$parent !== null && $scope.$parent.login){
              $scope.$parent.login($scope.registration.email, $scope.registration.password);
            }
            $scope.error = false;
            $scope.showSuccess = true;
          }
          $scope.isRunningRegister = false;
        }
      );
    }
  }

})();
