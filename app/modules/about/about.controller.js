;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AboutController', AboutController);

  AboutController.$inject = ['$scope'];

  function AboutController($scope) {
    // Page
    $scope.page.setTitle('Sobre');
    $scope.page.setDescription();
  }
})();
