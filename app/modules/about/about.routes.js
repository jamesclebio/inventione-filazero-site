;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .config(aboutRoutes);

  aboutRoutes.$inject = [
    '$stateProvider'
  ];

  function aboutRoutes($stateProvider) {
    $stateProvider
      .state('app.about', {
        url: '/about',
        templateUrl: 'modules/about/about.html',
        controller: 'AboutController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/about/about.controller.js'
              ]);
            });
          }]
        }
      });
  }
})();
