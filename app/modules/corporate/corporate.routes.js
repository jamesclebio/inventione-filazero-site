;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .config(corporateRoutes);

  corporateRoutes.$inject = ['$stateProvider'];

  function corporateRoutes($stateProvider) {
    $stateProvider
      .state('app.corporate', {
        url: '/corporate',
        templateUrl: 'modules/corporate/corporate.html',
        controller: 'CorporateController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/corporate/corporate.controller.js',
                'services/email.service.js'
              ]);
            });
          }]
        }
      });
  }
})();
