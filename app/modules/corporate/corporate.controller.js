;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('CorporateController', CorporateController);

  CorporateController.$inject = [
    '$scope',
    'EmailService'
  ];

  function CorporateController($scope, EmailService) {

    // Page
    $scope.page.setTitle('Empresas');
    $scope.page.setDescription();
    $scope.formSent = false;

    // Scrollink
    $(document).on({
      click: function (e) {
        scrollink($(this).data('scrollink'));
        e.preventdefault();
      }
    }, '[data-scrollink]');

    function scrollink(target) {
      var $target = $(target);

      $('body').animate({
        scrollTop: $target.offset().top - 44
      });
    }

    function initContactModel() {
      $scope.contact = {
        email: '',
        name: '',
        phone: '',
        message: ''
      };
    }

    initContactModel();

    $scope.sendContactMessage = function () {
      if (!$scope.contactForm.$invalid) {
        $scope.contact.subject = 'Contato - Filazero para negócios';
        $scope.isLoading = true;

        EmailService.send($scope.contact).then(
          function success(data) {
            if (data.messages[0].type == "SUCCESS") {
              initContactModel();
              $scope.contactForm.$setUntouched();
              $scope.formSent = true;
            }
            $scope.isLoading = false;
          },
          function (data) {
            $scope.isLoading = false;
          });
      }
    }
  }
})();
