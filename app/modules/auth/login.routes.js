﻿;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .config(loginRoutes);

  loginRoutes.$inject = ['$stateProvider'];

  function loginRoutes($stateProvider) {
    $stateProvider
      .state('app.login', {
        url: '/login?redirectTo',
        data:{
          requireAuthentication: false
        },
        templateUrl: 'modules/auth/login.html',
        controller: 'LoginController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
            ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/auth/login.controller.js',
                'services/auth.service.js',
                'services/auth-interceptor.service.js'
              ]);
            });
          }]
        }
      });
  }

}());
