﻿;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .controller('LoginSignUpModalController', LoginSignUpModalController);

  LoginSignUpModalController.$inject = [
    '$scope',
    'AuthService',
    '$modalInstance',
    '$timeout'
  ];

  function LoginSignUpModalController($scope, AuthService, $modalInstance, $timeout) {
    var currentTab = "login";

    $scope.setActiveTab = function (tab) {
      currentTab = tab;
    };

    $scope.isActiveTab = function (tab) {
      return currentTab === tab;
    };

    // Login
    $scope.login = function (userName, password) {
      $timeout(
        function () {
          $scope.loginData = {
            userName: userName,
            password: password
          };
          $scope.isRunningLogin = true;
          AuthService.login($scope.loginData).then(
            function (response) {
              $modalInstance.close();
              $scope.isRunningLogin = true;
            });
        }
        , 3000);
    };
  }

}());
