﻿;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .controller('LoginController', LoginController);

  LoginController.$inject = [
    '$scope',
    'AuthService',
    '$state',
    '$stateParams',
    'ngAuthSettings',
    '$location'
  ];

  function LoginController($scope, AuthService, $state, $stateParams, ngAuthSettings, $location) {

    $scope.loginData = {
      userName: "",
      password: ""
    };

    AuthService.fillAuthData();

    if (AuthService.authentication.isAuth)
      $state.go('app.account.overview');

    $scope.login = function () {
      if($scope.loginData && $scope.loginData.userName && $scope.loginData.password){
        $scope.isRunningLogin = true;
        AuthService.login($scope.loginData).then(
          function (response) {
            afterLogin();
          },
          function (error) {
            $scope.isRunningLogin = false;
          });
      }
    };

    $scope.authExternalProvider = function (provider) {

      var redirectUri = location.protocol + '//' + location.host + '/assets/plugins/auth-complete/auth-complete.html';

      var externalProviderUrl = ngAuthSettings.apiServiceBaseUri + "api/Account/ExternalLogin?provider=" + provider
        + "&response_type=token&client_id=" + ngAuthSettings.clientId
        + "&redirect_uri=" + redirectUri;
      window.$windowScope = $scope;

      var oauthWindow = window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
    };

    $scope.authCompletedCB = function (fragment) {

      $scope.$apply(function () {

        var externalData = {provider: fragment.provider, externalAccessToken: fragment.external_access_token};

        AuthService.registerOrUpdateExternal(externalData).then(
          function (response) {
            afterLogin();
          },
          function (err) {
            $scope.message = err.error_description;
          });

      });
    };

    function afterLogin(){
      $scope.isRunningLogin = false;
      if($scope.$parent.$close){
        $scope.$parent.$close();
      }else{
        var redirect = $stateParams.redirectTo;
        if(angular.isUndefined(redirect)){
          $state.go('app.account.overview');
          $scope.isRunningLogin = false;
        }

        $location.url(redirect);
      }
    }

  }

}());
