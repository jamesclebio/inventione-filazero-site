;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.terms', {
      url: '/terms',
      templateUrl: 'modules/terms/terms.html'
    });
  }
}());
