;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .config(exploreRoutes);

  exploreRoutes.$inject = ['$stateProvider'];

  function exploreRoutes($stateProvider) {
    $stateProvider
      .state('app.exploresearch', {
        url: '/explore?:search',
        templateUrl: 'modules/explore/explore.html',
        controller: 'ExploreController',
        reloadOnSearch: false,
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/explore/explore.controller.js',
                'services/explore.service.js'
              ]);
            });
          }]
        }
      })
      .state('app.explore', {
        url: '/explore',
        templateUrl: 'modules/explore/explore.html',
        controller: 'ExploreController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/explore/explore.controller.js',
                'services/explore.service.js'
              ]);
            });
          }]
        }
      });
  }

}());
