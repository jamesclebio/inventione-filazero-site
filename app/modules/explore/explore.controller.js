;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('ExploreController', ExploreController);

  ExploreController.$inject = [
    '$scope',
    'ExploreService',
    '$state',
    '$stateParams',
    '$location'
  ];

  function ExploreController($scope, ExploreService, $state, $stateParams, $location) {
    $scope.page.setTitle('Explore');
    $scope.page.setDescription();

    $scope.hasLocations = false;
    $scope.initialState = true;
    $scope.lastSearchString = '';
    $scope.hasMoreResults = true;
    var offset;
    var limit;

    $scope.search = function (string) {
      if($scope.lastSearchString !== string && string.length > 0){
        $scope.lastSearchString = string;
        if (string !== undefined && string !== '') {
          $location.search('search', string);
          offset = 0;
          limit = 9;
          $scope.isLoading = true;
          ExploreService.search(string, limit, offset).then(
            function (response) {
              $scope.locations = response.locations;
              $scope.hasMoreResults = response.locationsFound > $scope.locations.length;
              $scope.hasLocations = $scope.locations.length > 0;
              $scope.initialState = false;
              $scope.isLoading = false;
            },
            function () {
              $scope.isLoading = false;
            });
        }else{
          $scope.initialState = false;
          $scope.hasLocations = false;
        }
      }
    };

    function init() {
      if ($stateParams.search) {
        $scope.searchString = $stateParams.search;
        $scope.search($scope.searchString);
      }
    }

    init();

    $scope.moreResults = function () {
      offset += limit;
      ExploreService.search($scope.lastSearchString, limit, offset).then(
        function (response) {
          if (response.locations.length > 0) {
            $scope.locations = $scope.locations.concat(response.locations);
            $scope.hasMoreResults = response.locationsFound > $scope.locations.length;
          }
          else {
            $scope.hasMoreResults = false;
          }
        });
    };

    $scope.isMobileLocation = function (location) {
      return location.type === "Mobile";
    };
  }
})();
