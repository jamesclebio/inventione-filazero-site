;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .controller('FeedbackController', FeedbackController);

  FeedbackController.$inject = [
    '$scope',
    'FeedbackService',
    '$stateParams',
    'toaster'
  ];

  function FeedbackController($scope, FeedbackService, $stateParams, toaster) {

    // Page
    $scope.page.setTitle('Avaliação');
    $scope.page.setDescription();

    $scope.isLoading = true;
    $scope.hasUpdated = false;
    $scope.rated = false;

    $scope.feedback = {
      id: $stateParams.feedbackId,
      guid: $stateParams.guid,
      rate: 0,
      comment: ""
    };

    $scope.getFeedback = function () {
      FeedbackService.getFeedback($scope.feedback.id, $scope.feedback.guid).then(
        function (response) {
          response.ticket.emittedAt = new Date(response.ticket.emittedAt);
          response.ticket.changedStatus = new Date(response.ticket.changedStatus);
          $scope.ticket = response.ticket;
          $scope.hasResource = !!$scope.ticket.resource;
          if (response.lastUpdate) {
            response.lastUpdate = new Date(response.lastUpdate);
            $scope.hasUpdated = true;
            $scope.feedback.rate = response.rate;
            $scope.rate = response.rate;
            $scope.feedback.comment = response.comment;
            $scope.feedback.lastUpdate = response.lastUpdate
          }
          else
            $scope.hasUpdated = false;
          $scope.isLoading = false;
        },
        function (error) {
          $scope.isLoading = false;
        }
      );
    };

    $scope.sendFeedback = function () {
      if($scope.feedback.rate > 0){
        FeedbackService.feedback($scope.feedback).then(
          function () {
            $scope.rated = true;
          },
          function () {
            $scope.error = true;
          }
        );
      }else{
        toaster.pop({
          type: 'error',
          body: "Atribua uma nota ao seu atendimento"
        });
      }
    };

    $scope.changeRate = function () {
      if ($scope.feedback.lastUpdate)
        $scope.feedback.rate = $scope.rate;
    };

    $scope.getFeedback();

  }

}());
