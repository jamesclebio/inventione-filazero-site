;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .config(feedbackRoutes);

  feedbackRoutes.$inject = ['$stateProvider'];

  function feedbackRoutes($stateProvider) {
    $stateProvider
      .state('app.feedback', {
        url: '/feedback/:feedbackId?:guid',
        params: {
          feedbackId: "",
          guid: ""
        },
        templateUrl: 'modules/feedback/feedback.html',
        controller: 'FeedbackController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/feedback/feedback.controller.js',
                'services/feedback.service.js'
              ]);
            });
          }]
        }
      })
  }

}());
