; (function () {
  'use strict';

  angular
    .module('filazero-site').
    controller('TicketDetailsModalController', TicketDetailsModalController);

  TicketDetailsModalController.$inject = [
    'TicketsService',
    '$state',
    '$modalInstance',
    'resolve',
    'Pusher'
  ];

  function TicketDetailsModalController(TicketsService, $state, $modalInstance, resolve, Pusher) {
    var vm = this;

    vm.isLoading = true;
    vm.closeModal = closeModal;
    vm.getTicket = getTicket;
    vm.hasTicketActions = hasTicketActions;
    vm.confirmTicket = confirmTicket;
    vm.checkTicket = checkTicket;
    vm.cancelTicket = cancelTicket;
    vm.canConfirmTicket = canConfirmTicket;
    vm.canCheckTicket = canCheckTicket;
    vm.canCancelTicket = canCancelTicket;
    vm.canSendFeedback = canSendFeedback;
    vm.sendFeedback = sendFeedback;
    vm.hasFeedback = hasFeedback;

    var ticketId = resolve.ticketId;

    init();

    function init() {
      getTicket(ticketId);
    }

    // Ticket
    function getTicket(ticketId) {
      vm.isLoading = true;
      TicketsService.getTicket(ticketId).then(
        function (response) {
          vm.ticket = response;
          subscribePusher();
          vm.isLoading = false;
        });
    }

    function isCompleted() {
      if (vm.ticket) {
        return vm.ticket.status.toUpperCase() === 'COMPLETED';
      }
    }
    // /Ticket

    // Actions
    function hasTicketActions() {
      if (vm.ticket) {
        return vm.ticket.actions.length > 0;
      }
      return false;
    }

    function canConfirmTicket() {
      if (vm.ticket) {
        return canDoSomething(vm.ticket, 'confirm');
      }
      return false;
    }

    function canCheckTicket() {
      if (vm.ticket) {
        return canDoSomething(vm.ticket, 'check');
      }
      return false;
    }

    function canCancelTicket() {
      if (vm.ticket) {
        return canDoSomething(vm.ticket, 'cancel');
      }
      return false;
    }

    function canDoSomething(ticket, action){
      for (var i = 0; i < ticket.actions.length; i++) {
        if (ticket.actions[i].toLowerCase() === action.toLowerCase()) {
          return true;
        }
      }
    }

    function confirmTicket() {
      doSomeAction(vm.ticket.provider.id, vm.ticket.id, 'confirm');
    }

    function checkTicket() {
      doSomeAction(vm.ticket.provider.id, vm.ticket.id, 'check');
    }

    function cancelTicket() {
      doSomeAction(vm.ticket.provider.id, vm.ticket.id, 'cancel');
    }

    function doSomeAction(providerId, ticketId, action){
      vm.isLoadingAction = true;
      TicketsService.ticketAction(providerId, ticketId, action).then(
        function (response) {
          getTicket(ticketId);
          vm.isLoadingAction = false;
        }
      );
    }
    // /Actions

    // Feedback
    function hasFeedback() {
      if (vm.ticket && vm.ticket.feedback) {
        return !!vm.ticket.feedback.rate;
      }
    }

    function canSendFeedback() {
      if (vm.ticket) {
        return (isCompleted() && !hasFeedback());
      }
    }

    function sendFeedback(ticket) {
      $modalInstance.dismiss('cancel');
      $state.go('app.feedback', { feedbackId: ticket.feedback.id, guid: ticket.feedback.guid });
    }
    // /Feedback

    // Pusher
    function subscribePusher() {
      Pusher.subscribe('ticket-' + vm.ticket.id, 'PrevisionChanged', function (data) {
        vm.ticket.estimatedServiceStart = data.EstimatedServiceStart;
      });
    }
    // /Pusher

    function closeModal() {
      $modalInstance.dismiss('cancel');
    }
  }
})();
