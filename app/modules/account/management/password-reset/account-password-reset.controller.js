;
(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AccountPasswordResetController', AccountPasswordResetController);

  AccountPasswordResetController.$inject = ['AccountService', '$stateParams'];

  function AccountPasswordResetController(AccountService, $stateParams) {

    var vm = this;

    vm.hasError = false;
    vm.resetedPassword = false;
    vm.passwordObject = {
      userId: $stateParams.u,
      token: $stateParams.c
    };

    vm.resetPassword = resetPassword;

    function resetPassword(passwordObject) {
      vm.isLoadingResetPassword = true;
      AccountService.resetPassword(passwordObject).then(
        function (response) {
          if (response.messages[0].type !== "SUCCESS") {
            vm.hasError = true;
            if(response.messages[0].code === '2060'){
              vm.invalidRequestToken = true;
            }
          } else {
            vm.hasError = false;
            vm.resetedPassword = true;
          }
          vm.isLoadingResetPassword = false;
        });
    }
  }
}());
