; (function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AccountProfileController', AccountProfileController);

  AccountProfileController.$inject = ['AccountService'];

  function AccountProfileController(AccountService) {

    var vm = this;

    vm.isLoading = true;
    vm.user = {};
    vm.selected = {}
    vm.datePickerStatus = false;
    vm.maxDate = new Date();
    vm.days = [];
    vm.months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
    vm.years = [];

    vm.updateUser = updateUser;

    init();

    function init() {
      getUserInfo();
      for (var i = 1; i <= 31; i++) {
        vm.days.push(i);
      }
      for (var i = 1930; i <= 2000; i++) {
        vm.years.push(i);
      }
    }

    function getUserInfo() {
      vm.isLoading = true;
      AccountService.userInfo().then(
        function (user) {
          vm.user = user;
          vm.selected.day = vm.user.birthDateDay;
          vm.selected.month = vm.months[vm.user.birthDateMonth - 1];
          vm.selected.year = vm.user.birthDateYear;

          vm.isLoading = false;
        });
    }

    function updateUser(user, form) {
      if(form.$valid){
        vm.isRunningUpdateUser = true;

        user.birthDateDay = vm.selected.day;
        user.birthDateMonth = vm.months.indexOf(vm.selected.month) + 1;
        user.birthDateYear = vm.selected.year;

        AccountService.updateUser(user).then(
          function () {
            vm.isRunningUpdateUser = false;
          }
        );
      }
    }

  }
})();
