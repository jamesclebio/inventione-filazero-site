;
(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AccountPasswordForgotController', AccountPasswordForgotController);

  AccountPasswordForgotController.$inject = ['AccountService'];

  function AccountPasswordForgotController(AccountService) {
    var vm = this;

    vm.hasError = false;
    vm.requestedNewPassword = false;
    vm.email = '';

    vm.forgotPassword = forgotPassword;

    function forgotPassword(email) {
      vm.isRunningForgotPassword = true;
      AccountService.forgotPassword(email).then(
        function(response){
          if(response.messages){
            vm.hasError = true;
            vm.error = response.messages[0].description;
          }else{
            vm.hasError = false;
            vm.requestedNewPassword = true;
          }
          vm.isRunningForgotPassword = false;
        }
      )
    }

  }
}());
