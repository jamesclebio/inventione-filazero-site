; (function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AccountPasswordChangeController', AccountPasswordChangeController);

  AccountPasswordChangeController.$inject = ['AccountService'];

  function AccountPasswordChangeController(AccountService) {

    var vm = this;

    vm.passwordObject = {};

    vm.changePassword = changePassword;
    vm.checkPassword = checkPassword;
    vm.isSocialAccount = isSocialAccount;

    init();

    function init() {
      getAccountData();
    }

    function changePassword(passwordObject, form) {
      if(form.$valid){
        vm.isRunningChangePassword = true;
        AccountService.changePassword(passwordObject).then(
          function (response) {
            if (response.messages[0].type === 'SUCCESS') {
              vm.passwordObject = {};
              form.$setUntouched();
              form.$submitted = false;
            }
          }).finally(
          function () {
            vm.isRunningChangePassword = false;
          });
      }
    }

    function checkPassword() {
      return vm.passwordObject.newPassword === vm.passwordObject.confirmNewPassword;
    }

    function getAccountData() {
      AccountService.userInfo().then(
        function (response) {
          vm.account = response;
        }
      );
    }

    function isSocialAccount() {
      if (vm.account) {
        return vm.account.isSocialAccount;
      }
      return false;
    }
  }
})();
