;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('OverbookingCancelController', OverbookingCancelController);

  OverbookingCancelController.$inject = ['$stateParams', 'OverbookService'];

  function OverbookingCancelController($stateParams, OverbookService) {

    var vm = this;

    vm.cancelled = false;
    vm.isLoading = true;

    init();

    function cancel(overbookId){
      vm.isLoading = true;
      OverbookService.cancel(overbookId).then(
        function(){
          vm.cancelled = true;
          vm.isLoading = false;
        },
        function(){
          vm.cancelled = false;
          vm.isLoading = false;
        }
      )
    }

    function init() {
      cancel($stateParams.overbookId);
    }

  }
}());
