; (function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AccountConfirmController', AccountConfirmController);

  AccountConfirmController.$inject = [
    'AuthService',
    '$stateParams',
    '$state'
  ];

  function AccountConfirmController(AuthService, $stateParams, $state) {
    var vm = this;

    vm.isLoading = false;
    var userId = $stateParams.u;
    var token = $stateParams.c;

    init();

    function init(){
      if (userId && token) {
        var data = {
          'userId': userId,
          'token': token
        };

        vm.isLoading = true;
        AuthService.validateEmail(data).then(
          function (response) {
            if (!response.hasOwnProperty('messages')) {
              if(AuthService.authentication.isAuth){
                AuthService.refreshToken().then(
                  function(){
                    vm.user = response.user;
                    vm.accountConfirmed = true;
                    vm.isLoading = false;
                  }
                );
              }else{
                vm.isLoading = false;
                vm.user = response.user;
                vm.accountConfirmed = true;
              }
            } else {
              if (response.messages[0].code == 3026) {
                vm.accountAlreadyConfirmed = true;
              }
            }
          });
      }else{
        $state.go('app.home');
      }
    }

  }
})();
