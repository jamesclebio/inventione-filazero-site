;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .config(accountRoutes);

  accountRoutes.$inject = ['$stateProvider'];

  function accountRoutes($stateProvider) {
    $stateProvider
      .state('app.account', {
        abstract: 'true',
        url: '/account',
        data: {
          requireAuthentication: true
        },
        redirectTo: 'app.account.overview',
        templateUrl: 'modules/account/account.html',
        controller: 'AccountController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/account.controller.js'
              ]);
            });
          }]
        }
      })
      .state('app.account.activities', {
        url: '/activities',
        templateUrl: 'modules/account/info/activities/account-activities.html',
        controller: 'AccountActivitiesController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'select',
              'services/tickets.service.js',
              'services/activities.service.js',
              'services/auth.service.js',
              'services/account.service.js'
            ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/info/activities/account-activities.controller.js',
                'modules/tickets/ticket-details-modal.controller.js'
              ]);
            });
          }]
        }
      })
      .state('app.account.overview', {
        url: '/overview',
        templateUrl: 'modules/account/info/overview/account-overview.html',
        controller: 'AccountOverviewController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/info/overview/account-overview.controller.js',
                'services/account.service.js'
              ]);
            });
          }]
        }
      })
      .state('app.account.password-change', {
        url: '/change-password',
        templateUrl: 'modules/account/management/password-change/account-password-change.html',
        controller: 'AccountPasswordChangeController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/management/password-change/account-password-change.controller.js',
                'services/account.service.js'
              ]);
            });
          }]
        }
      })
      .state('app.account.profile', {
        url: '/profile',
        templateUrl: 'modules/account/management/profile/account-profile.html',
        controller: 'AccountProfileController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load(['select'], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/management/profile/account-profile.controller.js',
                'services/account.service.js'
              ]);
            });
          }]
        }
      })
      .state('app.account.tickets', {
        url: '/tickets',
        templateUrl: 'modules/account/info/tickets/account-tickets.html',
        controller: 'AccountTicketsController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'filters/start-from.filter.js',
                'services/account.service.js',
                'services/tickets.service.js',
                'services/auth.service.js',
                'modules/account/info/tickets/account-tickets.controller.js',
                'modules/tickets/ticket-details-modal.controller.js'
              ]);
            });
          }]
        }
      })
      .state('app.confirm', {
        url: '/account/confirm?u&c',
        templateUrl: 'modules/account/confirm/account-confirm.html',
        controller: 'AccountConfirmController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load(['select'], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'services/auth.service.js',
                'modules/account/confirm/account-confirm.controller.js',
              ]);
            });
          }]
        }
      })
      .state('app.password-forgot', {
        url: '/account/forgot-password',
        templateUrl: 'modules/account/management/password-forgot/account-password-forgot.html',
        controller: 'AccountPasswordForgotController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/management/password-forgot/account-password-forgot.controller.js',
                'services/account.service.js'
              ]);
            });
          }]
        }
      })
      .state('app.password-reset', {
        url: '/account/reset-password?u&c',
        templateUrl: 'modules/account/management/password-reset/account-password-reset.html',
        controller: 'AccountPasswordResetController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/management/password-reset/account-password-reset.controller.js',
                'services/account.service.js'
              ]);
            });
          }]
        }
      })
      .state('app.overbooking-cancel', {
        url: '/overbooking/:overbookId/cancel',
        templateUrl: 'modules/account/overbooking/overbooking-cancel.html',
        controller: 'OverbookingCancelController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/overbooking/overbooking-cancel.controller.js',
                'services/overbooking.service.js'
              ]);
            });
          }]
        }
      });
  }
})();
