; (function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AccountController', AccountController);

  AccountController.$inject = [
    '$scope',
    '$state'
  ];

  function AccountController($scope, $state) {
    var vm = this;
    var currentMenu;

    vm.setActiveMenu = setActiveMenu;
    vm.isActiveMenu = isActiveMenu;

    init();

    function init() {
      checkState();
    }

    function checkState() {
      var currentState = $state.current.name;
      if (currentState) {
        currentState = currentState.slice(currentState.lastIndexOf(".") + 1, currentState.length);
        if (currentState === 'account') {
          setActiveMenu('overview');
        } else {
          setActiveMenu(currentState);
        }
      }
    }

    $scope.$on('$stateChangeSuccess', function () {
      checkState();
    });

    function setActiveMenu(menu) {
      currentMenu = menu;
    }

    function isActiveMenu(menu) {
      return currentMenu === menu;
    }
  }
})();
