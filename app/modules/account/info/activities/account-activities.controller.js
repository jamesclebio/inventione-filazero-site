;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AccountActivitiesController', AccountActivitiesController);

  AccountActivitiesController.$inject = [
    'ActivitiesService',
    '$modal',
    'AuthService',
    'AccountService'
  ];

  function AccountActivitiesController(ActivitiesService, $modal, AuthService, AccountService) {

    var vm = this;

    vm.isLoading = true;
    vm.currentPage = 1;
    vm.itensLimit = 10;
    vm.order = '-dateTime';
    vm.searchActivity = '';
    vm.totalActivities = 0;

    vm.filteredActivities = [];
    vm.activities = [];
    vm.options = [
      5,
      10,
      20,
      50
    ];

    vm.changePage = changePage;
    vm.setOrder = setOrder;
    vm.ticketsDetails = ticketsDetails;
    vm.resendConfirmationEmail = resendConfirmationEmail;

    init();

    function changePage() {
      getActivities(vm.itensLimit, (vm.currentPage - 1) * vm.itensLimit);
    }

    function init() {
      AuthService.getLocalUserInfo().then(
        function (response) {
          if (response.emailConfirmed) {
            getActivities(vm.itensLimit, (vm.currentPage - 1) * vm.itensLimit);
          } else {
            vm.isLoading = false;
            vm.emailNotConfirmed = true;
          }
        }
      );
    }

    function getActivities(limit, offset) {
      vm.isLoading = true;
      ActivitiesService.getUserActivities(limit, offset).then(
        function (response) {
          vm.activities = response.activities;
          vm.totalActivities = response.totalActivities;
        },
        function (error) {

        }).finally(function () {
        vm.isLoading = false;
      });
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }

    function ticketsDetails(ticketId) {
      $modal.open({
        templateUrl: 'modules/tickets/ticket-details-modal.html',
        controller: 'TicketDetailsModalController',
        controllerAs: 'vm',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return {
              ticketId: ticketId
            };
          }
        }
      });
    }

    function resendConfirmationEmail() {
      vm.isSendingEmail = true;
      AuthService.getLocalUserInfo().then(
        function (response) {
          AccountService.resendConfirmation(response.userName).then(
            function(response){
              vm.emailSent = response.messages[0].type === 'SUCCESS';
            }
          ).finally(
            function(){
              vm.isSendingEmail = false;
            }
          );
        });
    }
  }
})();
