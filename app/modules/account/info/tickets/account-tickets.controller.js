; (function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AccountTicketsController', AccountTicketsController);

  AccountTicketsController.$inject = [
    'AccountService',
    '$modal',
    'AuthService'
  ];

  function AccountTicketsController(AccountService, $modal, AuthService) {
    var vm = this;

    vm.isLoading = true;
    vm.currentPage = 1;
    vm.itensLimit = "5";
    vm.order = 'estimatedStart';
    vm.showActiveTickets = true;
    vm.showCompletedTickets = false;
    vm.showCanceledTickets = false;
    vm.tickets = [];

    vm.canShowTicket = canShowTicket;
    vm.concatLists = concatLists;
    vm.getTicket = getTickets;
    vm.setOrder = setOrder;
    vm.showTicketDetails = showTicketDetails;
    vm.resendConfirmationEmail = resendConfirmationEmail

    init();

    function canShowTicket() {
      return function (ticket) {
        switch (ticket.status.toUpperCase()) {
          case 'COMPLETED':
            if (vm.showCompletedTickets) {
              return true;
            }
            break;
          case 'CANCELLED':
            if (vm.showCanceledTickets) {
              return true;
            }
            break;
          default:
            if (vm.showActiveTickets) {
              return true;
            }
            break;
        }
        return false;
      };
    }

    function concatLists(){
      var list = [];

      if(vm.showActiveTickets){
        list = vm.lists.active;
      }

      if(vm.showCanceledTickets){
        list = list.concat(vm.lists.cancelled);
      }

      if(vm.showCompletedTickets){
        list = list.concat(vm.lists.completed);
      }

      if(vm.showExpiredTickets){
        list = list.concat(vm.lists.expired);
      }

      vm.tickets = list;
    }

    function getTickets() {
      vm.isLoading = true;
      AccountService.getTickets().then(
        function (response) {
          vm.lists = response;
          vm.hasTickets = false;
          angular.forEach(vm.lists, function(list){
            if(list.length > 0){
              vm.hasTickets = true;
            }
          });
          concatLists();
          vm.isLoading = false;
        });
    }

    function init() {
      AuthService.getLocalUserInfo().then(
        function(response){
          if(response.emailConfirmed){
            getTickets();
          }else{
            vm.isLoading = false;
            vm.emailNotConfirmed = true;
          }
        },
        function(error){
          console.log(JSON.stringify(error));
        }
      );
    }

    function showTicketDetails(ticketId) {
      $modal.open({
        templateUrl: 'modules/tickets/ticket-details-modal.html',
        controller: 'TicketDetailsModalController',
        controllerAs: 'vm',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return {
              ticketId: ticketId
            };
          }
        }
      });
    }

    function resendConfirmationEmail() {
      vm.isSendingEmail = true;
      AuthService.getLocalUserInfo().then(
        function (response) {
          AccountService.resendConfirmation(response.userName).then(
            function(response){
              vm.emailSent = response.messages[0].type === 'SUCCESS';
            }
          ).finally(
            function(){
              vm.isSendingEmail = false;
            }
          );
        });
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }
  }
})();
