;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AccountOverviewController', AccountOverviewController);

  AccountOverviewController.$inject = [
    'AccountService'
  ];

  function AccountOverviewController(AccountService) {
    var vm = this;

    vm.isLoading = true;
    vm.ticketsCount = {};
    vm.getAccountData = getAccountData;

    init();

    function init() {
      getAccountData();
    }

    function getAccountData() {
      vm.isLoading = true;

      AccountService.userInfo().then(function (response) {
        vm.account = response;
        if(vm.account.birthDateYear && vm.account.birthDateMonth && vm.account.birthDateDay){
          vm.account.birthDate = new Date(vm.account.birthDateYear, vm.account.birthDateMonth - 1, vm.account.birthDateDay);
        }
        vm.isLoading = false;
      });
    }
  }
})();
