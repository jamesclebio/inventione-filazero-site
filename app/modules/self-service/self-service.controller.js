;
(function() {
  'use strict';

  angular
    .module('filazero-site', ['vcRecaptcha'])
    .controller('SelfServiceController', SelfServiceController);

  SelfServiceController.$inject = [
    '$scope',
    '$stateParams',
    'ProviderService',
    'SelfServiceService',
    'OverbookService',
    '$q',
    'AuthService',
    '$modal',
    'vcRecaptchaService'
  ];

  function SelfServiceController($scope, $stateParams, ProviderService, SelfServiceService, OverbookService, $q, AuthService, $modal, recaptchaService) {
    var providerSlug = $stateParams.providerSlug;
    var locationId = $stateParams.locationId;

    var datesAvailability = [];
    $scope.focusPhone = false;
    $scope.isLoading = true;
    $scope.isLoadingDatesAvailability = true;
    $scope.isAvailable = false;
    $scope.overbookData = {};

    $scope.overbooked = false;
    $scope.selected = {};
    $scope.userInfo = {};

    // Watchers
    $scope.$watch('location', function(newValue, oldValue) {
      if (newValue) {
        $scope.hasServices = $scope.location.services.length > 0;
      }
    });

    $scope.$watch('activeService', function(newValue, oldValue) {
      if (newValue) {
        getDatesAvailability(providerSlug, $scope.activeService.id);
        resetServiceStates();
      }
    });

    $scope.$watch('selected.date', function(newValue, oldValue) {
      if (newValue) {
        if ($scope.activeService) {
          getSessionsAvailability($scope.activeService.id);
        }
        resetServiceStates();
      }
    });

    $scope.$watch('selected.resource', function(newValue, oldValue) {
      if (newValue) {
        $scope.resourceHasSessions = hasSessionForSelectedResource();
      }
    });

    $scope.$watch('selected.session', function(newValue, oldValue) {
      if (newValue) {
        $scope.focusPhone = !$scope.focusPhone;
      }
    });

    $scope.$watch('availability', function(newValue, oldValue) {
      if (newValue) {
        $scope.hasResources = $scope.availability.resources.length > 0;
        $scope.hasSessions = $scope.availability.sessions.length > 0;
      }
    });
    // /Watchers

    // Recaptcha
    $scope.recaptchaListeners = {
      onCreate: function(widgetId) {
        $scope.recaptchaWidgetId = widgetId;
      },

      onResponse: function(response) {
        $scope.recaptchaResponse = response;
      },

      onExpiration: function() {
        $scope.recaptchaResponse = null;
        recaptchaService.reload($scope.recaptchaWidgetId);
      }
    };
    // Recaptcha

    // Locations
    var getLocation = function(providerSlug, locationId) {
      $scope.isLoading = true;
      ProviderService.getProvider(providerSlug, locationId).then(
        function(data) {
          $scope.location = data;
          $scope.isLoading = false;
          if ($stateParams.s && $stateParams.d && $stateParams.ses) {
            for (var i = 0; i < data.services.length; i++) {
              if (data.services[i].id == $stateParams.s) {
                $scope.activeService = data.services[i];
                break;
              }
            }
          }
        });
    };
    // /Locations

    // Disponibilities
    var getSessionsAvailability = function(serviceId) {
      $scope.isLoadingAvailability = true;
      $scope.availability = {
        resources: [],
        sessions: []
      };
      var hasSlotsLeft = false;
      var selectedDate = '';
      for (var i = 0; i < datesAvailability.length; i++) {
        selectedDate = datesAvailability[i].date;
        if ($scope.selected.date.toDateString() === selectedDate.toDateString() && datesAvailability[i].hasSlotLeft) {
          hasSlotsLeft = true;
          break;
        }
      }
      if (hasSlotsLeft) {
        SelfServiceService.getSessionsAvailability(providerSlug, locationId, serviceId, $scope.selected.date).then(
          function(data) {
            $scope.availability = data;
            $scope.availability.resources.unshift({
              "id": 0,
              "name": "Sem preferência"
            });
            if ($stateParams.s && $stateParams.d && $stateParams.ses) {
              for (var i = 0; i < $scope.availability.sessions.length; i++) {
                if ($scope.availability.sessions[i].id == $stateParams.ses) {
                  $scope.selected.session = $scope.availability.sessions[i];
                  break;
                }
              }
            }
            $scope.selected.resource = $scope.availability.resources[0];
            if ($scope.availability.sessions) {
              for (i = 0; i < $scope.availability.sessions.length; i++) {
                $scope.availability.sessions[i].nextAttendance = new Date($scope.availability.sessions[i].nextAttendance);
                $scope.availability.sessions[i].start = new Date($scope.availability.sessions[i].start);
              }
            }

            $scope.isLoadingAvailability = false;
          },
          function(error) {
            $scope.isLoadingAvailability = false;
          });
      } else {

        $scope.isLoadingAvailability = false;
      }
    };

    var getDatesAvailability = function(providerSlug, serviceId) {
      $scope.isLoadingDatesAvailability = true;
      SelfServiceService.getDatesAvailability(providerSlug, serviceId).then(
        function(data) {
          datesAvailability = data;
          for (var i = 0; i < datesAvailability.length; i++) {
            datesAvailability[i].date = new Date(datesAvailability[i].date);
          }
          if ($stateParams.s && $stateParams.d && $stateParams.ses) {
            $scope.selected.date = new Date($stateParams.d);
          } else {
            $scope.selected.date = new Date();
          }
          $scope.isLoadingDatesAvailability = false;
        },
        function(error) {
          $scope.isLoadingDatesAvailability = false;
        });
    };

    $scope.goNextDate = function() {
      for (var i = 0; i < datesAvailability.length; i++) {
        if (datesAvailability[i].hasSlotLeft) {
          $scope.selected.date = datesAvailability[i].date;
          break;
        }
      }
    };

    $scope.hasAvailableDays = function() {
      return datesAvailability.length > 0;
    };
    // /Disponibilities

    // Services
    $scope.setActiveService = function(service) {
      if (!$scope.activeService || $scope.activeService.id !== service.id) {
        $scope.activeService = service;
      } else {
        unsetActiveService();
      }
    };

    var unsetActiveService = function() {
      $scope.activeService = undefined;
    };

    $scope.isActiveService = function(service) {
      if ($scope.activeService) {
        return $scope.activeService.id === service.id;
      }
      return false;
    };

    // /Services

    // Resources
    var hasSessionForSelectedResource = function() {
      if ($scope.selected.resource && $scope.selected.resource.id === 0) {
        return true;
      }
      for (var i = 0; i < $scope.availability.sessions.length; i++) {
        if ($scope.availability.sessions[i].resourceId === $scope.selected.resource.id) {
          return true;
        }
      }
      return false;
    };

    $scope.hasSessionForResource = function(resource) {
      if (resource.id === 0) {
        return true;
      }
      for (var i = 0; i < $scope.availability.sessions.length; i++) {
        if ($scope.availability.sessions[i].resourceId === resource.id) {
          return true;
        }
      }
      return false;
    };

    $scope.showRadio = function(session) {
      if ($scope.hasSelectedResource()) {
        if ($scope.selected.resource.id === 0) {
          return true;
        }
        return $scope.isSessionOfSelectedResource(session);
      } else {
        return true;
      }
    };

    $scope.isSessionOfSelectedResource = function(session) {
      if ($scope.hasSelectedResource()) {
        return session.resourceId === $scope.selected.resource.id;
      }
      return false;
    };

    $scope.resourceSelected = function(session) {
      for(var i = 0; i < $scope.availability.resources.length; i++){
        if($scope.availability.resources[i].id === session.resourceId){
          return $scope.availability.resources[i].name
        }
      }
      return "Compartilhada";
    };

    $scope.hasSelectedResource = function() {
      return $scope.selected.resource;
    };
    // /Resources

    // Sessions
    $scope.hasSelectedSession = function() {
      return !!$scope.selected.session;
    };

    $scope.isSessionExpired = function(session) {
      var sessionEnd = new Date(session.end);
      return sessionEnd.getTime() < new Date().getTime();
    };
    // /Sessions

    // User
    var getUserInfo = function() {
      AuthService.profileInfo().then(
        function(data) {
          $scope.userInfo = data.user;
          $scope.overbookData = {
            name: $scope.userInfo.firstName,
            email: $scope.userInfo.userName,
            phone: $scope.userInfo.phone
          };
        }
      );
    };
    // /User

    // Datepicker
    $scope.getDayClass = function(date, mode) {
      if (mode === 'day') {
        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
        var currentDay;
        for (var i = 0; i < datesAvailability.length; i++) {
          currentDay = new Date(datesAvailability[i].date).setHours(0, 0, 0, 0);

          if (dayToCheck === currentDay) {
            if (datesAvailability[i].hasSlotLeft) {
              return 'available';
            } else {
              return 'unavailable';
            }
          }
        }
      }
    };
    // /Datepicker

    // Overbook
    $scope.canOverbook = function() {
      return $scope.overbookData.name && $scope.overbookData.email && $scope.overbookData.phone;
    };

    $scope.overbook = function() {
      $scope.isRunningOverbook = true;
      $scope.overbookData.serviceId = $scope.activeService.id;

      OverbookService.addOverbook($scope.overbookData).then(
        function(data) {
          $scope.isRunningOverbook = false;
          if (data.messages[0].type === "ERROR") {
            $scope.overbooked = false;
          } else {
            $scope.isUser = data.messages[0].parameters.isUser;
            $scope.overbooked = true;
          }
        },
        function() {
          $scope.isRunningOverbook = false;
        }
      )
    };
    // /Overbook

    // Ticket
    $scope.canEmitTicket = function() {
      if ($scope.selected.session && $scope.userInfo.phone) {
        return $scope.selected.session && $scope.userInfo.phone;
      }
      return false;
    };

    $scope.tryEmitTicket = function(isValidForm) {
      var that = this;

      if ($scope.canEmitTicket() && isValidForm) {
        if (AuthService.authentication.isAuth) {
          emitTicket();
          this.selfServiceForm.$setPristine();
        } else {
          var modalInstance = $modal.open({
            templateUrl: 'modules/auth/login-signup-modal.html',
            controller: 'LoginSignUpModalController',
            size: 'md',
            windowClass: 'stick-up'
          });

          modalInstance.result.then(
            function() {
              emitTicket();
              that.selfServiceForm.$setPristine();
            });
        }
      } else {
        if (!this.userInfo.phone){
          $scope.focusPhone = !$scope.focusPhone;
        }
      }
    };

    var emitTicket = function() {
      $scope.isRunningEmitTicket = true;
      SelfServiceService.emitTicket($scope.location.provider.id, $scope.activeService.id, $scope.selected.session.id, $scope.selected.date, JSON.parse($scope.selected.special), $scope.userInfo.phone, $scope.recaptchaResponse).then(
        function(data) {
          $scope.ticketEmitted = data;
          if ($scope.ticketEmitted && $scope.ticketEmitted.id) {
            $scope.wasTicketEmitted = true;
            getUserInfo();
          }
          $scope.isRunningEmitTicket = false;
          recaptchaService.reload($scope.recaptchaWidgetId);
        });
    };

    $scope.showTicketDetails = function(ticketId) {
      $modal.open({
        templateUrl: 'modules/tickets/ticket-details-modal.html',
        controller: 'TicketDetailsModalController',
        controllerAs: 'vm',
        windowClass: 'stick-up',
        resolve: {
          resolve: function() {
            return {
              ticketId: ticketId
            };
          }
        }
      });
    };
    // /Ticket

    var resetServiceStates = function() {
      $scope.selected.resource = undefined;
      $scope.selected.session = undefined;
      $scope.resourceHasSessions = true;
      $scope.wasTicketEmitted = false;
      $scope.overbooked = false;
    };

    $scope.resetStates = function() {
      $scope.todayDate = new Date();
      $scope.selected.special = "false";
      $scope.selected.date = new Date();
      $scope.wasTicketEmitted = false;
      $scope.overbooked = false;
    };

    $scope.init = function() {
      getLocation(providerSlug, locationId);
      $scope.resetStates();
      if (AuthService.authentication.isAuth) {
        getUserInfo();
      }
    };

    $scope.init();
  }
})();
