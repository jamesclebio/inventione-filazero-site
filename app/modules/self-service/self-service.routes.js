;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .config(selfServiceRoutes);

  selfServiceRoutes.$inject = ['$stateProvider'];

  function selfServiceRoutes($stateProvider) {
    $stateProvider
      .state('app.provider', {
        url: '/p/:providerSlug/:locationId?s&d&ses',
        templateUrl: 'modules/self-service/self-service.html',
        controller: 'SelfServiceController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'switchery',
              'select',
              'inputMask',
              'recaptcha',
              'services/provider.service.js',
              'services/self-service.service.js',
              'services/tickets.service.js',
              'services/overbooking.service.js',
              'directives/focus-me.directive.js'
            ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/self-service/self-service.controller.js',
                'modules/tickets/ticket-details-modal.controller.js',
                'modules/auth/login-signup-modal.controller.js',
                'modules/auth/login.controller.js',
                'modules/signup/signup.controller.js'
              ]);
            });
          }]
        }
      })
  }

}());
