;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['$scope', '$state', 'AuthService'];

  function HomeController($scope, $state, AuthService) {
    // Page
    $scope.page.setTitle('Espere menos, faça mais.');
    $scope.page.setDescription();

    AuthService.fillAuthData();

    if (AuthService.authentication.isAuth)
      $state.go('app.account.overview');
  }
})();
