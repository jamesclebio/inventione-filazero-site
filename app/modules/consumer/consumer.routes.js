;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .config(consumerRoutes);

  consumerRoutes.$inject = ['$stateProvider'];

  function consumerRoutes($stateProvider) {
    $stateProvider
      .state('app.consumer', {
        url: '/consumer',
        templateUrl: 'modules/consumer/consumer.html',
        controller: 'ConsumerController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/consumer/consumer.controller.js'
              ]);
            });
          }]
        }
      });
  }
})();
