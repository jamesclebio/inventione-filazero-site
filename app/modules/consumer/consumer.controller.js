;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('ConsumerController', ConsumerController);

  ConsumerController.$inject = ['$scope'];

  function ConsumerController($scope) {
    // Page
    $scope.page.setTitle('Você');
    $scope.page.setDescription();
  }
})();
