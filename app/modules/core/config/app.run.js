;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .run(runConfig);

  runConfig.$inject = ['tmhDynamicLocale', '$rootScope', '$location', '$state', 'AuthService'];

  function runConfig(tmhDynamicLocale, $rootScope, $location, $state, AuthService){
    tmhDynamicLocale.set('pt-br');

    $rootScope.$on('$stateChangeStart', function (event, toState) {
      AuthService.fillAuthData();
      if (toState.data && toState.data.requireAuthentication && !AuthService.authentication.isAuth) {
        var redirectTo = encodeURIComponent($location.url());
        event.preventDefault();
        $state.transitionTo('app.login', { 'redirectTo': redirectTo});
      }
    });
  }
}());

