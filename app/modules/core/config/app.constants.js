;(function(){
  'use strict';

  // Service base
  window.serviceBase = (function () {
    var hosts = {
      production: {
        regex: /filazero\.net/,
        set: {
          clientId: 'filazeroSite',
          apiServiceBaseUri: 'https://api.filazero.net/',
          urlOrigin: 'https://app.filazero.net/#/',
          site: 'https://filazero.net/'
        }
      },

      release: {
        regex: /fzsitehmg\.azurewebsites\.net/,
        set: {
          clientId: 'filazeroSite-hmg',
          apiServiceBaseUri: 'https://fzapihmg.azurewebsites.net/',
          urlOrigin: 'https://fzapphmg.azurewebsites.net/#/',
          site: 'https://fzsitehmg.azurewebsites.net/#/'
        }
      },

      development: {
        regex: /fzsitedev\.azurewebsites\.net/,
        set: {
          clientId: 'filazeroSite-dev',
          apiServiceBaseUri: 'https://fzapidev.azurewebsites.net/',
          urlOrigin: 'https://fzappdev.azurewebsites.net/#/',
          site: 'http://fzsitedev.azurewebsites.net/#/'
        }
      },

      local: {
        regex: /localhost/,
        port: '3000',
        set: {
          clientId: 'filazeroSite-local',
          apiServiceBaseUri: 'https://localhost:44300/',
          urlOrigin: 'https://fzappdev.azurewebsites.net/#/',
          site: 'https://fzsitedev.azurewebsites.net/#/'
        }
      },

      localRemote: {
        regex: /localhost/,
        port: '37374',
        set: {
          clientId: 'filazeroApp-localRemote',
          apiServiceBaseUri: 'https://fzapihmg.azurewebsites.net/',
          urlOrigin: 'https://fzappdev.azurewebsites.net/#/',
          site: 'https://fzsitedev.azurewebsites.net/#/'
        }
      }
    };

    for (var host in hosts) {
      if (hosts[host].regex.test(window.location.hostname)) {

        if (!hosts[host].port) {
          return hosts[host].set;
        }

        if (hosts[host].port === window.location.port) {
          return hosts[host].set;
        }
      }
    }
  })();

  angular
    .module('filazero-site')
    .constant('ngAuthSettings', window.serviceBase);

}());
