;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .config(appConfig);

  appConfig.$inject = [
    '$httpProvider',
    'tmhDynamicLocaleProvider',
    'laddaProvider',
    '$translateProvider',
    'PusherServiceProvider'
  ];

  function appConfig($httpProvider, tmhDynamicLocaleProvider, laddaProvider, $translateProvider, PusherServiceProvider){
    $httpProvider.interceptors.push('authInterceptorService');
    tmhDynamicLocaleProvider.localeLocationPattern('assets/plugins/angular-i18n/angular-locale_{{locale}}.js');
    laddaProvider.setOption({ style: 'expand-left' });

    $translateProvider.useStaticFilesLoader({
      prefix: 'i18n/',
      suffix: '.json'
    });

    $translateProvider.preferredLanguage('pt-br');

    // Pusher configuration
    PusherServiceProvider.setToken('0041ab94e110de594bdb').setOptions({});
  }
}());
