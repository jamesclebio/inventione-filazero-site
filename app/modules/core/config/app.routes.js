(function () {
  'use strict';

  angular
    .module('filazero-site')
    .config(routeConfig);

  routeConfig.$inject = [
    '$stateProvider',
    '$urlRouterProvider'
  ];

  function routeConfig($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/app/home');

    $stateProvider
      .state('app', {
        abstract: true,
        url: '/app',
        templateUrl: 'layouts/base.html'
      })

      .state('app.home', {
        url: '/home',
        templateUrl: 'modules/home/home.html',
        controller: 'HomeController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/home/home.controller.js'
              ]);
            });
          }]
        }
      });
  }
})();
