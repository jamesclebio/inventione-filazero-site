;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .factory('RequestInterceptor', RequestInterceptor);

  RequestInterceptor.$inject = [
    '$q',
    'toaster'
  ];

  function RequestInterceptor($q, toaster) {
    return {
      responseError: function (response) {
        if (response.status) {
          if (response.status === 400) {
            if (response.data.error === '2061') {
              toaster.pop({
                type: 'error',
                body: response.data.error_description,
                showCloseButton: true
              });
            }
          }
        }

        return $q.reject(response);
      },

      response: function (response) {
        var messagesToDisplay = ['1008', '1028', '1046', '2004', '2005', '2056', '2059', '2097', '2123', '2138', '2144', '3024'];


        if (response.config.method !== 'GET' && response.status === 200) {
          if (response.data.messages && response.data.messages.length > 0) {
            var i, msg, type, title;

            for (i = 0; i <= response.data.messages.length - 1; i++) {
              if (messagesToDisplay.indexOf(response.data.messages[i].code) > -1) {

                msg = response.data.messages[i];
                switch (response.data.messages[i].type) {
                  case 'SUCCESS':
                    type = 'success';
                    title = 'Sucesso';
                    break;
                  case 'ERROR':
                    type = 'error';
                    title = 'Erro';
                    break;
                  case 'INFO':
                    type = 'info';
                    title = 'Informação';
                    break;
                  case 'WARNING':
                    type = 'warning';
                    title = 'Aviso';
                    break;
                }

                toaster.pop({
                  type: type,
                  body: msg.description,
                  showCloseButton: true
                });
              }
            }
          }
          // else if (!response.data.access_token) { // Not show for login success
          //   toaster.pop({
          //     type: 'success',
          //     body: 'Operação realizada com sucesso',
          //     showCloseButton: true
          //   });
          // }
        }

        return response || $q.when(response);
      }
    };
  }

  angular
    .module('filazero-site')
    .config([
      '$httpProvider',

      function ($httpProvider) {
        $httpProvider.interceptors.push('RequestInterceptor');
      }
    ]);

}());
