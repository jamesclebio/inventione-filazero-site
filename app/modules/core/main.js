;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('AppController', AppController);

  AppController.$inject = [
    '$scope',
    '$rootScope',
    '$state'
  ];

  function AppController($scope, $rootScope, $state) {

    // App globals
    $scope.app = {
      name: 'Filazero'
    };

    // Checks if the given state is the current state
    $scope.is = function (name) {
      return $state.is(name);
    };

    // Checks if the given state/child states are present
    $scope.includes = function (name) {
      return $state.includes(name);
    };

    // Broadcasts a message to pgSearch directive to toggle search overlay
    $scope.showSearchOverlay = function () {
      $scope.$broadcast('toggleSearchOverlay', {
        show: true
      });
    };

    // Page
    $scope.page = {
      setTitle: function (text) {
        if (text) {
          $scope.page.title = $scope.app.name + ' - ' + text;
        } else {
          $scope.page.title = $scope.app.name;
        }
      },

      setDescription: function (text) {
        if (text) {
          $scope.page.description = text;
        } else {
          $scope.page.description = '';
        }
      }
    };
  }
})();
