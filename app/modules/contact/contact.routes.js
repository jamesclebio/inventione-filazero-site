;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .config(contactRoutes);

  contactRoutes.$inject = ['$stateProvider'];

  function contactRoutes($stateProvider) {
    $stateProvider
      .state('app.contact', {
        url: '/contact',
        templateUrl: 'modules/contact/contact.html',
        controller: 'ContactController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'inputMask'
            ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/contact/contact.controller.js',
                'services/email.service.js'
              ]);
            });
          }]
        }
      });
  }
})();
