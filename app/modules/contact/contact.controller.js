;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('ContactController', ContactController);

  ContactController.$inject = [
    '$scope',
    'EmailService'
  ];

  function ContactController($scope, EmailService) {

    // Page
    $scope.page.setTitle('Contato');
    $scope.page.setDescription();

    function initContactModel() {
      $scope.contact = {
        email: '',
        name: '',
        phone: '',
        message: ''
      };
    }

    initContactModel();

    $scope.sendContactMessage = function () {
      if (!$scope.contactForm.$invalid) {
        $scope.contact.subject = 'Contato - Filazero para consumidores';
        $scope.isLoading = true;

        EmailService.send($scope.contact).then(
          function success(data) {
            if (data.messages[0].type == "SUCCESS") {
              initContactModel();
              $scope.contactForm.$setUntouched();
              $scope.formSent = true;
            }
            $scope.isLoading = false;
          },
          function (data) {
            $scope.isLoading = false;
          });
      }
    };
  }
})();
