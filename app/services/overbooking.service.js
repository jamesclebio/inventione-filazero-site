;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .factory('OverbookService', OverbookService);

  OverbookService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings'
  ];

  function OverbookService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceOverbook = $resource(serviceBase + 'api/overbooking/:overbookId', {overbookId: '@overbookId'}, {});

    return {

      addOverbook: function(overbook){

        var deferred = $q.defer();

        resourceOverbook.save(overbook,
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      cancel: function(overbookId){
        var deferred = $q.defer();

        resourceOverbook.delete({overbookId: overbookId},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    }
  }
}());
