;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .factory('ExploreService', ExploreService);
  ExploreService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings'
  ];

  function ExploreService($resource, $q, ngAuthSettings) {
    'use strict';

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceExploreSearch = $resource(serviceBase + 'api/search', {}, {});

    return {
      search: function (string, limit, offset) {
        var deferred = $q.defer();

        resourceExploreSearch.get({query: string, limit: limit, offset: offset},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }

    }
  }
}());
