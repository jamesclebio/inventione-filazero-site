;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .factory('AccountService', AccountService);
  AccountService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings'
  ];

  function AccountService($resource, $q, ngAuthSettings) {
    'use strict';

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceUser = $resource(serviceBase + 'api/me', {}, {update: {method: 'PUT'}});
    var resourceUserPassword = $resource(serviceBase + 'api/me/password', {}, {update: {method: 'PUT'}});
    var resourceUserTickets = $resource(serviceBase + 'api/me/tickets', {});
    var resourceForgotPassword = $resource(serviceBase + 'api/account/forgot-password', {});
    var resourceResetPassword = $resource(serviceBase + 'api/account/reset-password', {});
    var resourceResendConfirmation = $resource(serviceBase + 'api/account/resend-confirmation-email', {});

    return {
      changePassword: function (passwordObject) {
        var deferred = $q.defer();

        resourceUserPassword.update({}, passwordObject,
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      forgotPassword: function (forgotEmail) {
        var deferred = $q.defer();

        resourceForgotPassword.save({}, {email: forgotEmail},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      resendConfirmation: function (userEmail) {
        var deferred = $q.defer();

        resourceResendConfirmation.save({}, {email: userEmail},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getTickets: function () {
        var deferred = $q.defer();

        resourceUserTickets.get({},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      resetPassword: function (passwordObject) {
        var deferred = $q.defer();

        resourceResetPassword.save({}, passwordObject,
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      updateUser: function (user) {
        var deferred = $q.defer();

        resourceUser.update({}, user,
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      userInfo: function () {
        var deferred = $q.defer();

        resourceUser.get({},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    };
  }
}());
