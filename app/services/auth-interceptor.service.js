;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .factory('authInterceptorService', authInterceptorService);

  authInterceptorService.$inject = ['$q', '$injector', '$location', 'localStorageService'];

  function authInterceptorService($q, $injector, $location, localStorageService) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

      config.headers = config.headers || {};

      var authData = localStorageService.get('authorizationData');
      if (authData) {
        config.headers.Authorization = 'Bearer ' + authData.token;
      }

      return config;
    };

    var _responseError = function (rejection) {
      if (rejection.status === 401 && $location.path() !== '/login') {
        var authService = $injector.get('AuthService');
        var redirectTo = encodeURIComponent($location.url());

        authService.logOut();
        $location.url('/app/login').search({'redirectTo': redirectTo});
      }
      return $q.reject(rejection);
    };

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
  }
})();
