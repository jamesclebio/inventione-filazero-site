;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .factory('AuthService', AuthService);

  AuthService.$inject = ['$rootScope', '$http', '$q', 'localStorageService', 'ngAuthSettings'];

  function AuthService($rootScope, $http, $q, localStorageService, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var authServiceFactory = {};

    var _authentication = {
      isAuth: false,
      userName: ""
    };

    var _externalAuthData = {
      provider: "",
      userName: "",
      externalAccessToken: ""
    };

    var _saveRegistration = function (registration) {

      _logOut();

      return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
        return response;
      });

    };

    var _token = function (data) {
      var deferred = $q.defer();

      $http.post(
          serviceBase + 'token',
        $.param(data),
        {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .success(function (response) {

          localStorageService.set('authorizationData', {
            token: response.access_token,
            userName: data.userName,
            refreshToken: response.refresh_token
          });

          _authentication.isAuth = true;
          _authentication.userName = data.userName;

          $rootScope.$broadcast('$$userAuthenticated');

          afterLogin().then(function(){
            deferred.resolve(response);
          });

        }).error(function (err, status) {
        _logOut();
        deferred.reject(err);
      });

      return deferred.promise;
    }

    var _login = function (loginData) {

      var data = {
        grant_type: "password",
        client_id: ngAuthSettings.clientId,
        userName: loginData.userName,
        password: loginData.password
      };

      return _token(data);

    };

    var _logOut = function () {

      localStorageService.remove('authorizationData');

      _authentication.isAuth = false;
      _authentication.userName = "";
    };

    var _fillAuthData = function () {

      var authData = localStorageService.get('authorizationData');
      if (authData) {
        _authentication.isAuth = true;
        _authentication.userName = authData.userName;
      }

    };

    var _refreshToken = function () {
      var deferred = $q.defer();
      var authData = localStorageService.get('authorizationData');

      if (authData && authData.refreshToken) {

        var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + ngAuthSettings.clientId;

        localStorageService.remove('authorizationData');

        $http.post(
            serviceBase + 'token',
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
          .success(function (response) {

            localStorageService.set('authorizationData', {
              token: response.access_token,
              userName: response.userName,
              refreshToken: response.refresh_token
            });

            afterLogin().then(
              function(){
                deferred.resolve();
              }
            );

          }).error(function (err, status) {
          _logOut();
          deferred.reject(err);
        });
      } else {
        deferred.resolve();
      }

      return deferred.promise;
    };

    var _obtainAccessToken = function (externalData) {

      var deferred = $q.defer();

      $http.get(serviceBase + 'api/account/ObtainLocalAccessToken', {
        params: {
          provider: externalData.provider,
          externalAccessToken: externalData.externalAccessToken
        }
      }).success(function (response) {

        localStorageService.set('authorizationData', {
          token: response.access_token,
          userName: response.userName,
          provider: response.provider
        });

        _authentication.isAuth = true;
        _authentication.userName = response.userName;

        $rootScope.$broadcast('$$userAuthenticated');

        deferred.resolve(response);

      }).error(function (err, status) {
        _logOut();
        deferred.reject(err);
      });

      return deferred.promise;

    };

    var _getExternalUserInfo = function (registerExternalData) {
      var deferred = $q.defer();

      $http.post(serviceBase + 'api/account/externalUserInfo', registerExternalData)
        .success(function (response) {
          deferred.resolve(response);
        })
        .error(function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    };

    var _registerOrUpdateExternal = function (registerExternalData) {

      var deferred = $q.defer();

      $http.post(serviceBase + 'api/account/registerorupdateexternallogin', registerExternalData).success(function (response) {

        localStorageService.set('authorizationData', {
          token: response.access_token,
          userName: response.userName,
          refreshToken: response.refresh_token
        });

        _authentication.isAuth = true;
        _authentication.userName = response.userName;

        $rootScope.$broadcast('$$userAuthenticated');

        afterLogin().then(function(){
          deferred.resolve(response);
        });

      }).error(function (err, status) {
        _logOut();
        deferred.reject(err);
      });

      return deferred.promise;

    };

    var _retrieveProfileInfo = function () {
      var authData = localStorageService.get('authorizationData');
      var deferred = $q.defer();

      if (authData) {
        $http.get(serviceBase + 'api/me/profile')
          .success(function (response) {
            deferred.resolve(response);
          })
          .error(function (err, status) {
            deferred.reject(err);
          });
      }
      return deferred.promise;
    };

    var _validateEmail = function (data) {
      var deferred = $q.defer();

      if (data) {
        $http.post(serviceBase + 'api/account/confirm', data)
          .success(function (response) {
            deferred.resolve(response);
          })
          .error(function (err, status) {
            deferred.reject(err);
          });
      }

      return deferred.promise;
    };

    var _getLocalUserInfo = function () {
      var deferred = $q.defer();

      var userData = localStorageService.get('authorizationData');
      if (userData) {
        deferred.resolve(userData);
      }else{
        deferred.reject();
      }
      return deferred.promise;
    };

    var afterLogin = function(){
      var deferred = $q.defer();

      $http.get(serviceBase + 'api/me/profile').then(
        function(response){
          var localStorageData = localStorageService.get('authorizationData');
          var userData = response.data.user;

          userData.token = localStorageData.token;
          userData.userName = localStorageData.userName;
          if(localStorageData.refreshToken){
            userData.refreshToken = localStorageData.refreshToken;
          }

          localStorageService.set('authorizationData', userData);
          deferred.resolve(userData);
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    };

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.profileInfo = _retrieveProfileInfo;
    authServiceFactory.refreshToken = _refreshToken;

    authServiceFactory.obtainAccessToken = _obtainAccessToken;
    authServiceFactory.externalAuthData = _externalAuthData;
    authServiceFactory.getExternalUserInfo = _getExternalUserInfo;
    authServiceFactory.registerOrUpdateExternal = _registerOrUpdateExternal;
    authServiceFactory.validateEmail = _validateEmail;
    authServiceFactory.getLocalUserInfo = _getLocalUserInfo;

    return authServiceFactory;
  }
})();
