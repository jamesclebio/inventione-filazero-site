;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .factory('InvitesService', InvitesService);

  InvitesService.$inject = ['$resource', '$q', 'ngAuthSettings'];

  function InvitesService($resource, $q, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceInvite = $resource(serviceBase + 'api/me/invitations/:token', {token: '@token'}, {});

    return {
      acceptInvite: function (token) {
        var deferred = $q.defer();

        resourceInvite.save({token: token},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          }
        );

        return deferred.promise;
      }
    }
  }
}());
