;(function(){angular
  .module('filazero-site')
  .factory('FeedbackService', FeedbackService);

  FeedbackService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings'
  ];

  function FeedbackService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceFeedback = $resource(serviceBase + 'api/feedback/:feedbackId?guid=:guid', {feedbackId: '@feedbackId', guid: '@guid'}, {'update': {method: 'PUT'}});

    return {

      feedback: function (feedback) {
        var deferred = $q.defer();

        resourceFeedback.update({feedbackId: feedback.id, guid: feedback.guid}, feedback,
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getFeedback: function (feedbackId, guid) {
        var deferred = $q.defer();

        resourceFeedback.get({feedbackId: feedbackId, guid: guid},
          function (feedback) {
            deferred.resolve(feedback);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }

    }
  }

}());
