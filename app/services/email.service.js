;(function () {
  'use strict';

  angular
    .module('filazero-site')
    .factory('EmailService', EmailService);

  EmailService.$inject = ['$http', '$q', 'ngAuthSettings'];

  function EmailService($http, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    return {
      send: function (email) {
        var deferred = $q.defer();

        $http.post(serviceBase + '/api/email', email)
          .success(function (data) {
            deferred.resolve(data);
          })
          .error(function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }

    }
  }
})();
