;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .factory('TicketsService', TicketsService);

  TicketsService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings'
  ];

  function TicketsService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceTickets = $resource(serviceBase + 'api/me/tickets', {}, {});
    var resourceTicket = $resource(serviceBase + 'api/tickets/:ticketId', {ticketId: '@ticketId'}, {});
    var resourceTicketAction = $resource(serviceBase + 'api/providers/:providerId/tickets/:ticketId/:action', {providerId: '@providerId', ticketId: '@ticketId', action: '@action'}, {});

    return {
      getTickets: function () {

        var deferred = $q.defer();

        resourceTickets.query(
          function (tickets) {
            deferred.resolve(tickets);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getTicket: function (ticketId) {

        var deferred = $q.defer();

        resourceTicket.get({ticketId: ticketId},
          function (tickets) {
            deferred.resolve(tickets);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      // Actions
      ticketAction: function(providerId, ticketId, action){
        var deferred = $q.defer();

        resourceTicketAction.save({providerId: providerId, ticketId: ticketId, action: action},
          function (response) {
            deferred.resolve(response);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    }
  }

}());
