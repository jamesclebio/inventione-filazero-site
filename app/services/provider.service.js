;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .factory('ProviderService', ProviderService);

  ProviderService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings'
  ];

  function ProviderService($resource, $q, ngAuthSettings) {
    'use strict';

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceProvider = $resource(serviceBase + 'api/public/providers/slug/:providerSlug', {providerSlug: '@providerSlug'}, {})

    return {
      getProvider: function (providerSlug, locationId) {
        var deferred = $q.defer();

        resourceProvider.get({providerSlug: providerSlug, locationId: locationId},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          }
        );

        return deferred.promise;
      }
    }
  }

}());
