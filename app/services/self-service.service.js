;(function(){
  'use strict';

  angular
    .module('filazero-site')
    .factory('SelfServiceService', SelfServiceService);

  SelfServiceService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings'
  ];

  function SelfServiceService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceSelfService = $resource(serviceBase + 'api/self-service/providers/:providerSlug/locations/:locationId/services/:serviceId/sessions-resources-by-service', {providerSlug: '@providerSlug', locationId: '@locationId', serviceId: '@serviceId'}, {});
    var resourceDatesAvailability = $resource(serviceBase + 'api/self-service/providers/:providerSlug/services/:serviceId/available-session-days', { serviceId: '@serviceId', providerSlug: '@providerSlug'});
    var resourceSelfServiceEmitTicket = $resource(serviceBase + 'api/providers/:providerId/self-service/sessions/:sessionId/tickets/forsite', {providerId: '@providerId', sessionId: '@sessionId'}, {});

    return {
      getSessionsAvailability: function (providerSlug, locationId, serviceId, date) {
        var deferred = $q.defer();

        resourceSelfService.get({providerSlug: providerSlug, locationId: locationId, serviceId: serviceId, date: date},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          }
        );

        return deferred.promise;
      },

      getDatesAvailability: function (providerSlug, serviceId) {
        var deferred = $q.defer();

        resourceDatesAvailability.query({providerSlug: providerSlug, serviceId: serviceId},
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          }
        );

        return deferred.promise;
      },

      emitTicket: function (providerId, serviceId, sessionId, date, special, phone, recaptcha) {
        var deferred = $q.defer();

        resourceSelfServiceEmitTicket.save({
            serviceId: serviceId,
            sessionId: sessionId,
            date: date,
            special: special,
            phone: phone,
            providerId: providerId,
            recaptcha: recaptcha
          },
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          }
        );

        return deferred.promise;
      }
    }

  }

}());
