;(function(){
  'use strict';

  angular
    .module('filazero-site-mock', ['filazero-site', 'ngMockE2E'])
    .run(function ($httpBackend) {
      'use strict';

      $httpBackend.whenGET(/api/).passThrough();
      $httpBackend.whenPOST(/api/).passThrough();
      $httpBackend.whenPUT(/api/).passThrough();
      $httpBackend.whenDELETE(/api/).passThrough();
      $httpBackend.whenGET(/token/).passThrough();
      $httpBackend.whenPOST(/token/).passThrough();

      $httpBackend.whenGET(/.*\.html/).passThrough();
      $httpBackend.whenGET(/.*\.json/).passThrough();
      $httpBackend.whenGET(/.*\.jpg/).passThrough();
    });

}());
