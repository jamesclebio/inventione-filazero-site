angular
  .module('filazero-site')
  .directive('floatMe', floatMe);

function floatMe($timeout, $parse) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var $ = window.jQuery;
      var _prevY;
      
      function update() {
        var w = $(window).scrollTop();
        var translateY = (w - element.offset().top) * (-0.1);
        var delay = 1000 / 1000; //in seconds
        var curve = 'ease';
        var maxTopTranslate = 40;
        var maxBottomTranslate = 300;

        if (maxTopTranslate === 0) {
          if (element.offset().top + element.outerHeight() < w) {
            return;
          } 
        }

        if (maxBottomTranslate === 0) {
          if (element.offset().top > w + $(window).height()) {
            return;
          }
        }

        if (_prevY < translateY) { // scroll down, element will hide from top
          if (maxTopTranslate !== 0 && Math.abs(translateY) > maxTopTranslate) {
            return;
          }
        } else {
          if (maxBottomTranslate !== 0 && Math.abs(translateY) > maxBottomTranslate) {
            return;
          }
        }

        element.css({
          'transition': 'transform ' + delay + 's ' + curve,
          'transform': 'translateY(' + translateY + 'px)',
        });

        _prevY = translateY;
      }

      $(window).bind('scroll', function () {
        update();
      });
      $(window).bind('load', function () {
        update();
      });
    }
  };
}
