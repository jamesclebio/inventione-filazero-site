;(function () {
  /*
   Use this directive together with ng-include to include a
   template file by replacing the placeholder element
   */
  angular
    .module('filazero-site')
    .directive('includeReplace', includeReplaceDirective);

  function includeReplaceDirective() {
    return {
      require: 'ngInclude',
      restrict: 'A',

      link: function (scope, el, attrs) {
        el.replaceWith(el.children());
      }
    };
  }
}());
