angular
  .module('filazero-site')
  .directive('focusMe', focusMe);

function focusMe($timeout, $parse) {
  return {
    link: function(scope, element, attrs) {
      var model = $parse(attrs.focusMe);
      scope.$watch(model, function() {
        $timeout(function() {
          element[0].focus();
        });
      });
    }
  };
}
