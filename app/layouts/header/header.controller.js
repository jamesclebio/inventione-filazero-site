(function () {
  'use strict';

  angular
    .module('filazero-site')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = [
    '$scope',
    'AuthService',
    '$state',
    '$rootScope',
    '$modal'
  ];

  function HeaderController($scope, AuthService, $state, $rootScope, $modal){
    AuthService.fillAuthData();
    $scope.authentication = AuthService.authentication;

    $scope.register = function () {
      $modal.open({
        templateUrl: 'modules/signup/signup-modal.html',
        controller: 'SignupModalController',
        controllerAs: 'vm',
        size: 'lg'
      });
    }

    $scope.logOut = function () {
      AuthService.logOut();
      $scope.goTo('app.home');
    };

    $scope.userInfo = function (){
      AuthService.profileInfo().then(
        function(response){
          $scope.user = response.user;
        }
      );
    };

    $rootScope.$on('$$userAuthenticated', function () {
      $scope.authentication = AuthService.authentication;
    });

    $scope.$watch('authentication', function(newValue, oldValue){
      if(newValue.isAuth){
        $scope.userInfo();
      }
    }, true);

    $scope.goTo = function(state){
      var el = $(this);

      var header = el.attr('data-pages-element');
      $('body').toggleClass('menu-opened');
      $('[data-pages="header-toggle"]').toggleClass('on');
      $state.go(state);
    }
  }
})();
