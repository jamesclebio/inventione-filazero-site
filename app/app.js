; (function () {
  'use strict';

  // App
  angular
    .module('filazero-site', [
      'ui.router',
      'ui.utils',
      'ui.bootstrap',
      'ui.mask',
      'oc.lazyLoad',
      'ngResource',
      'LocalStorageModule',
      'tmh.dynamicLocale',
      'toaster',
      'ngAnimate',
      'ngMessages',
      'angular-ladda',
      'pascalprecht.translate',
      'AngularPrint',
      'virtualPage',
      'doowb.angular-pusher'
    ]);
})();
