﻿;(function(){
  'use strict';

  angular
    .module("filazero-site")
    .filter('capitalize', capitalizeFilter);
  function capitalizeFilter() {
    return function (input, scope) {
      if (input) {
        if (input !== null) {
          input = input.toLowerCase();
        }
        return input.substring(0, 1).toUpperCase() + input.substring(1);
      } else {
        return null;
      }
    };
  }

}());
