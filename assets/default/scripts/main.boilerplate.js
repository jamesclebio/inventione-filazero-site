;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.boilerplate = {
    settings: {
      autoinit: true
    },

    init: function () {
      this.builder();
    },

    builder: function () {},

    handler: function () {}
  };
}(jQuery, this, this.document));
