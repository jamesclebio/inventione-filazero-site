;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.scrolltop = {
    settings: {
      autoinit: true
    },

    init: function () {
      this.builder();
      this.handler();
    },

    builder: function () {},

    handler: function () {
      var that = this,
          $scrolltop = $('#scrolltop');

      // Click
      $(document).on({
        click: function () {
          that.run();
        }
      }, '[data-scrolltop], #scrolltop, .header .menu a, .boxtab-nav a');

      // Scroll
      $(window).scroll(function() {
        if ($(window).scrollTop() >= 60) {
          $scrolltop.fadeIn(50);
        } else {
          $scrolltop.fadeOut(50);
        }
      });
    },

    run: function () {
      $('html, body').animate({
        scrollTop: 0
      }, 50);
    }
  };
}(jQuery, this, this.document));
