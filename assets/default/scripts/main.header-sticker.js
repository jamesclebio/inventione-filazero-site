;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.headerSticker = {
    settings: {
      autoinit: true,
      main: '[data-header-sticker]',
      affixClass: 'affix',
      affixTopClass: 'affix-top'
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this,
          $headerSticker = $(this.settings.main);

      $(window).scroll(function() {
        if ($(window).scrollTop() >= 60) {
          $headerSticker.removeClass(that.settings.affixTopClass).addClass(that.settings.affixClass);
        } else {
          $headerSticker.removeClass(that.settings.affixClass).addClass(that.settings.affixTopClass);
        }
      });
    }
  };
}(jQuery, this, this.document));
