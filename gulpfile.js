var gulp = require('gulp'),
    addsrc = require('gulp-add-src'),
    compass = require('gulp-compass'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect'),
    del = require('del'),
    filter = require('gulp-filter'),
    gulpif = require('gulp-if'),
    htmlmin = require('gulp-htmlmin'),
    imagemin = require('gulp-imagemin'),
    jshint = require('gulp-jshint'),
    cleanCss = require('gulp-clean-css'),
    plumber = require('gulp-plumber'),
    replace = require('gulp-replace'),
    rev = require('gulp-rev'),
    revReplace = require('gulp-rev-replace'),
    taskListing = require('gulp-task-listing'),
    uglify = require('gulp-uglify'),
    useref = require('gulp-useref');

// Default
gulp.task('default', taskListing);

// Dev
gulp.task('dev', ['connect:app', 'watch:dev']);
gulp.task('dev:local', ['connect:app:local']);

// Prod
gulp.task('prod', ['connect:build']);

// Connect
gulp.task('connect:app', function () {
  connect.server({
    root: 'app',
    port: 37374,
    livereload: true
  });
});

// Connect
gulp.task('connect:app:local', function () {
  connect.server({
    root: 'app',
    port: 3000,
    livereload: true
  });
});

gulp.task('connect:build', function () {
  connect.server({
    root: 'build',
    port: 37374
  });
});

// Watch
gulp.task('watch:dev', function () {
  gulp.watch('assets/default/scripts/**/*', ['scripts']);
  gulp.watch('assets/default/styles/**/*', ['styles']);
  gulp.watch('app/**/*.html', ['app:views']);
  gulp.watch(['app/**/*.js', '!app/assets/**/*.js'], ['app:scripts']);
});

// Fonts
gulp.task('fonts', function () {
  del('app/assets/fonts/*', function () {
    gulp.src([
        'assets/template/fonts/**/*',
        'assets/default/fonts/**/*',
        'app/assets/plugins/boostrapv3/fonts/**/*',
        'app/assets/plugins/font-awesome/fonts/**/*'
      ])
      .pipe(gulp.dest('app/assets/fonts'))
  });
});

// Images
gulp.task('images', function () {
  del('app/assets/images/*', function () {
    gulp.src('assets/default/images/**/*')
      .pipe(imagemin({
        optimizationLevel: 5
      }))
      .pipe(addsrc.append('assets/template/images/**/*'))
      .pipe(gulp.dest('app/assets/images'))
  });
});

// Styles
gulp.task('styles', function () {
  del('app/assets/styles/*', function () {
    gulp.src('assets/default/styles/**/*.sass')
      .pipe(plumber())
      .pipe(compass({
        style: 'compressed',
        sass: 'assets/default/styles',
        css: 'app/assets/styles',
        font: 'app/assets/fonts',
        image: 'app/assets/images'
      }))
      .pipe(addsrc.prepend([
        'assets/template/css/pages-frontend.css',
        'assets/template/css/pages-icons.css'
      ]))
      .pipe(concat('main.css'))
      .pipe(replace('img/', 'images/'))
      .pipe(replace('url\(http://', 'url(https://'))
      .pipe(gulp.dest('app/assets/styles'))
      .pipe(connect.reload());
  });
});

// Scripts
gulp.task('scripts', function () {
  del('app/assets/scripts/*', function () {
    gulp.src([
        'assets/default/scripts/main.*.js',
        'assets/default/scripts/main.js'
      ])
      .pipe(plumber())
      .pipe(jshint())
      .pipe(jshint.reporter('default'))
      .pipe(addsrc.prepend([
        'assets/template/js/pages.image.loader.js',
        'assets/template/js/pages.frontend.js'
      ]))
      .pipe(concat('main.js'))
      .pipe(uglify())
      .pipe(gulp.dest('app/assets/scripts'))
      .pipe(connect.reload());
  });
});

// App
gulp.task('app:views', function () {
  gulp.src('app/**/*.html')
    .pipe(connect.reload());
});

gulp.task('app:scripts', function () {
  gulp.src([
      'app/**/*.js',
      '!app/assets/**/*.js'
    ])
    .pipe(connect.reload());
});

// Build
gulp.task('build', function () {
  var filterRestore = {
        restore: true
      },

      filterViews = filter([
        'index.html',
        'layouts/**/*.html',
        'modules/**/*.html'
      ], filterRestore),

      filterStyles = filter([
        'assets/styles/**/*.css',
        '!assets/styles/main.css'
      ], filterRestore),

      filterScripts = filter([
        'assets/scripts/**/*.js',
        'layouts/**/*.js',
        'modules/**/*.js',
        '!modules/**/*.routes.js'
      ], filterRestore);

  del('build/*', function () {
    gulp.src('app/**/*')

      // Views
      .pipe(filterViews)
      .pipe(useref())
      .pipe(gulpif('*.html', htmlmin({
        collapseWhitespace: true
      })))
      .pipe(filterViews.restore)

      // Styles
      .pipe(filterStyles)
      .pipe(cleanCss({
        keepSpecialComments: 0
      }))
      .pipe(rev())
      .pipe(filterStyles.restore)

      // Scripts
      .pipe(filterScripts)
      .pipe(uglify({
        mangle: false
      }))
      .pipe(rev())
      .pipe(filterScripts.restore)

      .pipe(revReplace())
      .pipe(gulp.dest('build'));
  });
});
